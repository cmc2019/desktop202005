* branches

** 0


|    |         |
| 10 | 10-slim |
|    |         |
| 20 | nvidia  |
|    |         |
|    |         |


** 10



* 10-slim

** 0


|    | Dockerfile | Docker hub build |
| 10 |            |                  |


** 10


#+HEADER: :tangle Dockerfile
#+BEGIN_SRC sh


FROM debian:10-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    systemd \
    init \
    linux-image-amd64 \
    grub-pc-bin \
    grub2-common \
    iproute2 \
    pciutils \
    mc \
    acl \
    iputils-ping \
    less \
    nano \
    xz-utils \
    && apt clean 


#+END_SRC


docker pull cmch/mydesktop:10-slim



* nvidia

** 0

| 10 | status |
|    |        |
| 20 |        |

** 20

#+HEADER: :tangle Dockerfile
#+BEGIN_SRC sh


#  FROM debian:10-slim 
FROM cmch/mydesktop:10-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    xserver-xorg-video-dummy \
    xserver-xorg-input-void \
    xserver-xorg-core \
    xinit \
    x11-xserver-utils \
    xserver-xorg-input-mouse \
    xserver-xorg-input-kbd \
    xserver-xorg-input-libinput \
    spectrwm \
    scrot \
    software-properties-common && \
    add-apt-repository \
    "deb http://deb.debian.org/debian/ buster main contrib non-free" && \
    apt update && apt install -yq  \
    linux-headers-amd64 \
    nvidia-legacy-390xx-driver \
    && apt clean 

#+END_SRC






